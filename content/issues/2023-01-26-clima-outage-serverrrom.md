---
# description of the subject in one sentence
title: Outage climate control system ETZ/D/96.2 serverroom
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2023-01-26 09:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-01-26 10:45:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Storage
 - ISG Services

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Solved** {{< track "2023-01-26 10:45:00" >}} - Systems are recovered. Redundancy of the project storage is an independent issue, notice upon recovery of full redundancy will be given.

**Update** {{< track "2023-01-26 09:45:00" >}} - Climate control system is restored and temperature is going back to normal. Systems are mostly recovered. Full redundancy of project storage is not yet restored.

**Problem** {{< track "2023-01-26 09:35:00" >}} - An emergency shut down of several servers was necessary due to a failure of the climate control system in our server room. The temperature has been lowered enough currently to prevent further outages.
