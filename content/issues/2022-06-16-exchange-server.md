---
# description of the subject in one sentence
title: Unreachable ee.ethz.ch Email recipients over ID Exchange mailserver
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2022-06-16 15:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2022-06-16 16:45:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate
affected:
 - Others

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---

**Solved** {{< track "2022-06-16 16:45:00" >}} - The configuration issue has been resolved.

**Problem** {{< track "2022-06-16 15:00:00" >}} - Emails with ee.ethz.ch recipients sent over the ID Exchange Server do not reach destination. ID Exchange admins are working on fixing the problem.
