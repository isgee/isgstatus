---
# description of the subject in one sentence
title: Planned project/ archive storage downtime and client reboot
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2020-07-11 08:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2020-07-11 12:00:00
# severity level of the issue => [down|disrupted|notice]
severity: down
# affected service(s) defined in cstate
affected:
 - Storage

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2020-07-11 12:00:00" >}} - Migration has been completed, all services are back to operational state.

**Maintenance** {{< track "2020-07-11 08:00:00" >}} - Migration started, services are shutdown.

**Info** {{< track "2020-07-06 08:47:00" >}} - Planned maintenance work at 2020-07-11, 8:00-12:00. Project/ archive storage services (known under the names "ocean", "bluebay", "lagoon" and "benderstor") will not be available. ISG-managed Linux clients will be rebooted.
