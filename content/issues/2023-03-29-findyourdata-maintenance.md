---
# description of the subject in one sentence
title: Downtime of FindYourData and Self-Service Portal services due to server system updates
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2023-03-29 06:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-03-29 08:15:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Accounts
 - Storage
 - ISG Services


# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2023-03-29 08:15:00" >}} - System upgrade and related
checks are completed. Self-Service Portal job processing is enabled.

**Update** {{< track "2023-03-29 07:45:00" >}} - FindYourData services are back
online. Self-Service Portal will still only queue jobs without processing the
jobs. Remaining non-critical services are checked and brought back online.

**Info** {{< track "2023-03-28 10:40:00" >}} - On 2023-03-29 starting on
06:00 the server providing
[FindYourData](https://computing.ee.ethz.ch/Workstations/FindYourData)
and backend services for the [Self-Service
Portal](https://support.ee.ethz.ch) will be updated. During the update
there will be disruptions for the FindYourData access. The Self-Service
Portal will not process any queued jobs.
