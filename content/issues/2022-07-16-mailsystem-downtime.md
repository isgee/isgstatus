---
# description of the subject in one sentence
title: Planned ISG D-ITET mailsystem downtime because of IMAP server platform switch
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2022-07-16 07:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2022-07-16 11:00:00
# severity level of the issue => [down|disrupted|notice]
severity: down
# affected service(s) defined in cstate
affected:
 - E-Mail

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2022-07-16 11:00:00" >}} - The update of our IMAP server to a new platform has completed successfully. The mailsystem is available again.

**Maintenance** {{< track "2022-07-16 07:00:00" >}} - Update of our IMAP server to a new platform has started. Therefore the mail system will not be available for our customers.

**Info** {{< track "2022-07-09 10:00:00" >}} - On 2022-07-16 between 07:00 and 12:00 ISG is going to migrate the IMAP Server to another system. During the migration there will be no access to the E-Mails for the following domains:

  * ee.ethz.ch
  * vision.ee.ethz.ch
  * isi.ee.ethz.ch
  * mwe.ee.ethz.ch
  * mins.ee.ethz.ch
  * nari.ee.ethz.ch
