---
# description of the subject in one sentence
title: FindYourData/itet-stor frontend and self-service portal downtime 
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2024-10-01 06:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2024-10-01 07:15:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Accounts
 - Storage
 - ISG Services

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2024-10-01 07:15:00" >}} - Update is completed and
services are back to normal. ISG D-ITET is still doing final checks on the
server which should not impact user operations.

**Update** {{< track "2024-10-01 07:10:00" >}} - Access to the [Self-Service
Portal](https://support.ee.ethz.ch) is restored.

**Update** {{< track "2024-10-01 06:45:00" >}} - The
[FindYourData](https://computing.ee.ethz.ch/Workstations/FindYourData) is
already back in operation.

**Update** {{< track "2024-10-01 06:00:00" >}} - Upgrade started. Expect
several short downtimes for accessing the services. SSP does not accept new
jobs.

**Info** {{< track "2024-09-30 09:50:00" >}} - Due to planned
maintenance (upgrade host) on 2024-10-01 starting on 06:00 the server
providing
[FindYourData](https://computing.ee.ethz.ch/Workstations/FindYourData)
and backend services for the [Self-Service
Portal](https://support.ee.ethz.ch) will be updated. During the update
there will be disruptions for the FindYourData access. The Self-Service
Portal will not process any queued jobs.
