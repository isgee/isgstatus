---
# description of the subject in one sentence
title: Downtime RDS web client frontend and gateway
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date:  2023-02-02 07:15:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-02-02 09:35:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - ISG Services


# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2023-02-02 09:35:00" >}} - Maintenance for the server is completed and all services back online.

**Maintenance** {{< track "2023-02-02 07:15:00" >}} - ISG RDS web client frontend and gateway is down for maintenance.
