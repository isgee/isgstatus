---
# description of the subject in one sentence
title: Disruption on ID IAM service availability
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2023-08-03 13:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-08-03 16:50:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Accounts
 - Others

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Solved** {{< track "2023-08-03 16:50:00" >}} - Problem has been identified
and solved by the ID IAM team.

**Problem** {{< track "2023-08-03 13:30:00" >}} - ID IAM service is currently
disrupted and various operations on accounts could not be performed. This issue
has an impact on tasks triggered by the [self-service
portal](https://support.ee.ethz.ch) causing that account modifications might
fail. Until the problems have been resolved the self-service portal UI has been
disabled to disallow triggering new requests. We contacted ID for resolution of
this stability problem. 
