---
# description of the subject in one sentence
title: Project and archive storage downtime
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2023-06-17 08:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-06-17 12:40:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Accounts
 - Storage
 - Arton Cluster
 - Websites
 - ISG Services


# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2023-06-17 12:40:00" >}} - System is back online

**Update** {{< track "2023-06-17 12:25:00" >}} - [Self-service portal](https://support.ee.ethz.ch) and queues for the [CPU/GPU cluster](https://computing.ee.ethz.ch/Services/SLURM) are re-enabled.

**Update** {{< track "2023-06-17 12:00:00" >}} - Storage backend servers haven been updates and the storage filesystem version upgraded. Most storage attached clients are back online. System status is beeing reviewed.

**Maintenance** {{< track "2023-06-17 08:00:00" >}} - Update of our storage system for project and archive services has started.

**Info** {{< track "2023-05-17 15:00:00" >}} - Due to planned maintenance work, the project / archive services (known under the names *bluebay*, *lagoon*, *benderstor* and *beegfs02/scratch*) will not be available on Saturday, June 17 2023, starting from 8:00h and will last several hours.

We also have to reboot all ISG-managed Linux client workstations, e.g. personal clients, Tardis clients and Arton clients. Please save all your data before the maintenance work starts, disconnect any connections (also on self-managed devices) to the storage servers mentioned above and log out of your client. We recommend you to log out of your client session on Friday evening (June 16) after work. Furthermore disconnect all connected external storage devices from managed clients, e.g. USB sticks or USB disks.

Please note that other services which depend on the mentioned storage services, primarily the [CPU/GPU cluster](https://computing.ee.ethz.ch/Services/SLURM), login server and [cronbox](https://computing.ee.ethz.ch/Services/Cronjob), [self-service portal](https://support.ee.ethz.ch) and some websites, will also be unavailable during the downtime. 
