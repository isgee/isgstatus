---
# description of the subject in one sentence
title: HOME server maintenance to repair fileystem inconsistence
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2021-11-01 05:50:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2021-11-01 06:30:00
# severity level of the issue => [down|disrupted|notice]
severity: down
# affected service(s) defined in cstate
affected:
 - Storage

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2021-11-01 06:30:00" >}} - System back online and HOME directories are again accessible for all D-ITET user.

**Maintenance** {{< track "2021-11-01 05:50:00" >}} - HOME Server will be put offline to start a repair of a filesystem inconsistence.
