---
# description of the subject in one sentence
title: Power grid tests in ETZ building
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2023-02-16 06:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-02-13 13:10:30
# severity level of the issue => [down|disrupted|notice]
severity:
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Network
 - ISG Services


# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2023-02-13 13:10:30" >}} - The integral test scheduled
on Thursday, February 16, 2023 in the ETZ building must be postponed due to
time constraints. A new date is planned for summer/autumn 2023.

**Info** {{< track "2023-02-13 08:10:00" >}} - On Thursday, February 16, 2023
several integral power grid tests will be conducted from 06:00 to 12:00. During
the given timeframe one test will affect the whole ETZ building, which will be
without power for 10 minutes between 06:00 and 06:30. In further tests groups
of rooms will be up to two times without power (aimed to be completed around
07:00). Other tests do not impact the normal operations in the ETZ building and
affect specialized rooms (C64, D64 and D65).

The power for climate system and server room will be guaranteed by Facility
Services Department and we do not expect downtimes for running services.
Network connectivity might be disrupted.
