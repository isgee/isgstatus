---
# description of the subject in one sentence
title: Server shutdown due the integral safety test in ETZ
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2024-01-09 06:15:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2024-01-09 07:45:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Network
 - Storage
 - ISG Services

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2024-01-09 07:45:00" >}} - All servers and services are restored.

**Update** {{< track "2024-01-09 07:25:00" >}} - ETZ power has been restored for the ETZ serverroom and we are powering back the shutdown servers

**Update** {{< track "2024-01-09 07:15:00" >}} - Main power is still powered off, we are delaying powering on the non-critical systems.

**Info** {{< track "2024-01-08 14:50:00" >}} - On Tuesday, January 9, 2024
integral safety test in the ETZ building will be conducted.  The power supply
in D-ITET's server room is also interrupted from 06:30 to 07:00. To extent the
autonomous time of the UPS for important servers we will shut some of our
managed servers (starting at 06:15):

* `vela`
* `neon`
* `octopus[00-02]`
* `mwesrv01`
* `hybrida01`
* `Altium Server`

The shutdown will be last for about 30-45 minutes.

In the unlikely event, that the interruption should last longer, we will also
shutdown managed storage servers without further notice.
