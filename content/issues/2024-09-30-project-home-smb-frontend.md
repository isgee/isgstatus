---
# description of the subject in one sentence
title: Project homes SMB/CIFS frontend downtime
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2024-09-30 06:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2024-09-30 06:40:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Accounts
 - Storage

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2024-09-30 06:40:00" >}} - All user affecting services are now again up and running.

**Update** {{< track "2024-09-30 06:30:00" >}} - Parts of the upgrade impacting access to the project storage is finished.

**Update** {{< track "2024-09-30 06:00:00" >}} - Upgrade started. Expect several short downtimes for accessing the project storage through SMB/CIFS protocol.

**Info** {{< track "2024-09-26 15:40:00" >}} - Due to planned maintenance (upgrade host) access to the project homes through the SMB/CIFS protocol will not be available on Monday, September 30, starting from around 06:00 to 07:30.

Main impact is on managed Windows clients and for self-managed devices accessing the project storage through the SMB/CIFS protocol.

Please save all your data before the maintenance work starts, disconnect any connections from managed Windows and self-managed devices.
