---
# description of the subject in one sentence
title: Network outage ETx router
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2020-04-07 05:30:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2020-04-07 05:40:00
# severity level of the issue => [down|disrupted|notice]
severity: down
# affected service(s) defined in cstate
affected:
 - Network

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---

**Solved** {{< track "2020-04-07 05:40:00" >}} - There was an issue on the Router rou-etx. ID networking team trackled and solved the issue. There was about a 10min interuption for the ETx networking zone affecting almost all ISG D-ITET maintained systems.

**Problem** {{< track "2020-04-07 05:30:00" >}} - Network availability at ETX zone.
