---
# description of the subject in one sentence
title: NetScratch Server Hardware Maintenance
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2025-03-03 06:15:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2025-03-03 06:30:00
# severity level of the issue => [down|disrupted|notice]
severity:
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Storage
 - Arton Cluster
 - ISG Services

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2025-03-03 06:30:00" >}} - The defective hardware
component has been replaced and server is back online.

**Info** {{< track "2025-02-26 13:55:00" >}} - Due to planned hardware
maintenance (replacement of defective components) on 2025-03-03 starting from
06:15 the server providing the [NetScratch
service](https://computing.ee.ethz.ch/Services/NetScratch) will be offline.
During the maintenance access to the NetScratch filesystem is capped. Consider
this downtime in particular if you are running jobs in one of the Slurm
clusters.
