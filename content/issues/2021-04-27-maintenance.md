---
# description of the subject in one sentence
title: Downtime various D-ITET services for server maintenance
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2021-04-27 06:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2021-04-27 08:30:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate
affected:
 - ISG Services

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---



**Completed** {{< track "2021-04-27 08:30:00" >}} - Condor is back online, all services restored.

**Update** {{< track "2021-04-27 08:15:00" >}} - Matrix/Element Chat services back online.

**Update** {{< track "2021-04-27 08:00:00" >}} - Database upgrade done and online.

**Update** {{< track "2021-04-27 07:30:00" >}} - Slurm services are back online.

**Update** {{< track "2021-04-27 07:00:00" >}} - Base system has been upgraded, main database services in progress of upgrade.

**Maintenance** {{< track "2021-04-27 06:00:00" >}} - On 2021-04-27 between 06:00 and 08:30 ISG is going to update a server providing access to various D-ITET services. During the migration the following services will be affected and offline:

 * Matrix/Element Chat services (the instances will be unavailable)
 * IFA/Control Website: Access to the IFA database is blocked
 * Slurm (D-ITET Arton Cluster): It won't be possible to submit new jobs or view Slurm statistics. Already running jobs will not be affected.
 * Condor: Condor clients will be shut down the evening before to avoid running jobs during the migration.
