---
# description of the subject in one sentence
title: Network outage ETx area
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2022-10-27 06:30:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational: false
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2022-10-27 07:35:00
# severity level of the issue => [down|disrupted|notice]
severity: down
# affected service(s) defined in cstate
affected:
 - Network

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---
**Completed** {{< track "2022-10-27 07:35:00" >}} - ID-NET team confirmed end of maintenance. Systems are up and running.

**Update** {{< track "2022-10-27 07:30:00" >}} - ETx router switchover to the new hardware has been done, systems outside ETx zone need more time to recover from the switch.

**Update** {{< track "2022-10-27 07:20:00" >}} - Switchover to the new ETx router is delayed

**Maintenance** {{< track "2022-10-27 06:30:00" >}} - ID-NET team is starting with the planned update of the ETx router.

**Info** {{< track "2022-10-24 11:06:00" >}} - On Thursday, October 27 2022 from 06:30 to 07:00, the new router hardware in the network zone ETx will be commissioned. Affected by this interruption is the entire data network, wireless and IP telephony in the following buildings:

  * ETA
  * ETF
  * ETL
  * ETZ (inclusive server room ETZ D 96.2)
  * GLC
  * HCH
  * HCW
  * KUE
  * SHM
  * VOA
  * VOB
