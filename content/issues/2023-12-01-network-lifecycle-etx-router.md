---
# description of the subject in one sentence
title: Network interruption due to hardware lifecycle zone router ETx
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2023-12-01 19:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-12-01 23:05:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Accounts
 - Storage
 - E-Mail
 - Arton-Cluster
 - Websites
 - ISG Services
 - Network
 - LDAP/AD

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Solved** {{< track "2023-12-01 23:05:00" >}} - Systems should be back to
normal. There are some fallouts to ISG services from this second outage which
is worked on.

**Update** {{< track "2023-12-01 22:15:00" >}} - ID-NET working on resolving
the issue.

**Problem** {{< track "2023-12-01 22:00:00" >}} - There are again unexpected
downtimes to the network. Services are unreachable.

**Completed** {{< track "2023-12-01 21:00:00" >}} - Almost all services are
so far recovered. ISG is monitoring systems for further fallouts.

**Mainteannce** {{< track "2023-12-01 20:25:00" >}} - Systems are slowly
recovering. Some clients cannot be recovered and will need to be reset.

**Maintenance** {{< track "2023-12-01 20:10:00" >}} - ID-NET localized the
problem and working on the problem. Systems are coming back. ISG is monitoring
the system status and take action as needed for the fallouts in service
recovery.

**Maintenance** {{< track "2023-12-01 20:00:00" >}} - We were able to reach out
to the ID-NET. There is a problem with the switchover and the downtime
prolonged.

**Maintenance** {{< track "2023-12-01 19:35:00" >}} - Maintenance of ETx router
is still ongoing and systems are still not reachable. There are currently
unclear prolonged downtimes for the networking zone.

**Maintenance** {{< track "2023-12-01 19:00:00" >}} - ID-NET team started with
the router maintenance. Several services are now unreachable.

**Info** {{< track "2023-11-15 11:40:00" >}} - On Friday, December 1,
2023, between 19:00 and 22:00 ID network infrastructure team (ID-NET)
will perform maintenance to the **ETx** zone router affecting network
operation in the ETA, ETF, ETL, ETZ, VOA, VOB buildings (in the USZ -
KUE, SHM26 and Unispital switches) and the data center (DCETX) in the
ETZ. As part of the lifecycle maintenance the old ETx router will be
removed and all the switches connected to it moved to a new router
(already in operation).

During the maintenance window ID-NET expects an **interruption to the
whole data network** including telephony and WLAN of approximately
**15 minutes**.

Please account for not having access to various network related services
(e.g.  user homes, network attached storage, websites, email, Slurm
compute clusters, logins to clients) or stability of the clients.

All services should recover after the interruption depending on its
duration. In case of hanging clients a reboot will be necessary.
