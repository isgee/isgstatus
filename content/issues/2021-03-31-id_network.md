---
# description of the subject in one sentence
title: Network disruption affecting several ISG D-ITET services
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2021-03-31 07:30:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2021-03-31 09:30:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate
affected:
 - Network

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---



**Completed** {{< track "2021-04-27 08:30:00" >}} - The configuration error was found. The configuration change will be deployed on **2021-04-01 around 06:15** and a short network of about 1min is expected.

**Update** {{< track "2021-03-31 08:00:00" >}} - ID Networking team has rolled-back a deployed configuration, pending further investigation/analysis.

**Problem** {{< track "2021-03-31 07:30:00" >}} - There are currently disruption affecting a VPZ with servers managed by ISG D-ITET. Networking team of ID is investigating the issue. There are several ISG D-ITET services affected/malfunctioning due to this in particluar the [FindYourData](https://computing.ee.ethz.ch/Workstations/FindYourData) service.
