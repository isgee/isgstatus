---
# description of the subject in one sentence
title: User homes storage downtime
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2024-09-02 04:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2024-09-02 08:05:00
# severity level of the issue => [down|disrupted|notice]
severity:
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Accounts
 - Storage
 - Arton Cluster
 - Websites
 - ISG Services


# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2024-09-02 08:05:00" >}} - All user affecting services are now again up and running.

**Update** {{< track "2024-09-02 08:00:00" >}} - Access to [CPU/GPU cluster](https://computing.ee.ethz.ch/Services/SLURM) is released and queued pending jobs are now running.

**Update** {{< track "2024-09-02 07:50:00" >}} - [Self-service portal](https://support.ee.ethz.ch) is enabled.

**Update** {{< track "2024-09-02 07:15:00" >}} - Enabled SSH access through `login.ee.ethz.ch` and restarting of [cronbox](https://computing.ee.ethz.ch/Services/Cronjob) service.

**Update** {{< track "2024-09-02 07:10:00" >}} - Access via FindYourData/itet-stor is enabled.

**Update** {{< track "2024-09-02 06:55:00" >}} - New home server is up and running. NFS access from managed Linux machines is restored (some clients might require a reboot). Access from Windows is restored but users which were not completely logged out from their machine are required to sign-off and login again.

**Update** {{< track "2024-09-02 03:35:00" >}} - Preparation work for data migration is starting.

**Info** {{< track "2024-08-20 15:00:00" >}} - Due to planned maintenance (upgrade host and move to new infrastructure), the user homes will not be available on Monday, September 2nd 2024, starting from 04:00 and will last several hours (approximately until 09:00).

Each user process on ISG-managed Linux client workstations, e.g. personal clients, Tardis clients and Arton clients will be stopped and access to the home storage unmounted. This affects each process of an account of type stud, guest or staff. Please save all your data before the maintenance work starts, disconnect any connections (also on self-managed devices) to the home server and log out of your client. We recommend you to log out of your client session latest on Sunday evening (September 1st). Furthermore disconnect all connected external storage devices from managed clients, e.g. USB sticks or USB disks in case we will need to force reboot a client.

Please note that other services, primarily the [CPU/GPU cluster](https://computing.ee.ethz.ch/Services/SLURM), login server and [cronbox](https://computing.ee.ethz.ch/Services/Cronjob), [self-service portal](https://support.ee.ethz.ch) and personal websites, will also be unavailable during the downtime.
