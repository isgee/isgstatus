---
# description of the subject in one sentence
title: NetScratch server filesystem maintenance
# date/time the issue arised [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
date: 2022-07-09 13:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved [YYYY-MM-DD HH:MM:SS] (seconds are important, otherwise the render will fail!)
resolvedWhen: 2022-07-10 10:55:00
# severity level of the issue => [down|disrupted|notice]
severity: disrupted
# affected service(s) defined in cstate
affected:
 - Storage

# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counte measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a onging maintenance
# Completed   -> for a completed maintenace work
#
# each entry has a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}} after the "keyword" (seconds are important, otherwise the render will fail!)
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or recive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2022-07-10 10:55:00" >}} - Fileysstem for NetScratch is online and in read-write mode.

**Maintenance** {{< track "2022-07-10 09:30:00" >}} - First run of checks are completed and we are proceeding with the next steps to put the filesystem again online.

**Info** {{< track "2022-07-07 11:47:00" >}} - On 2022-07-09, 13:00 the NetScratch filesystem will be put into read-only mode for maintenance.
