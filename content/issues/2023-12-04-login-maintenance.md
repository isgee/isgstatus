---
# description of the subject in one sentence
title: Migration of login.ee.ethz.ch and cronbox.ee.ethz.ch services
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2023-12-04 06:00:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-12-04 06:30:00
# severity level of the issue => [down|disrupted|notice]
severity:
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - ISG Services


# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Completed** {{< track "2023-12-04 06:30:00" >}} - Migration to the new server has completed.

**Info** {{< track "2023-11-30 10:30:00" >}} -  The server hosting `login.ee.ethz.ch` as [gateway for remote access to the D-ITET infrastructure](https://computing.ee.ethz.ch/RemoteAccess#SSH_-_remote_terminal_session) (as alternative to VPN) and `cronbox.ee.ethz.ch` as described in the [D-ITET computing wiki](https://computing.ee.ethz.ch/Services/Cronjob) is being decommissioned and the services migrated on **Monday, December 4th, 2023 at 06:00**.

During the migration of the services login through `login.ee.ethz.ch` will not be possible and existing SSH connections will be terminated.

With the migration the services will be hosted by a **new server**.  While logging in please verify the [new SSH host key fingerprint](https://ssh-fingerprints.ee.ethz.ch/):

**ssh-rsa:**
```
MD5:85:40:de:3f:65:55:c7:92:f6:5b:31:4c:94:c7:0b:3d
SHA256:takJm61sfbNGp8CArgQB2V5jJdB/QrQcPxVaILQYbI8
```

**ssh-ed25519:**
```
MD5:ee:fb:3e:35:bb:fe:6f:bf:fb:3c:d6:17:a9:53:04:29
SHA256:1uPUoqFXpzcFYlLnYYXKDzyKo9GI7u2EQ+re4mu0vKs
```
