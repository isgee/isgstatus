---
# description of the subject in one sentence
title: NetScratch filesystem corruption
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2024-01-24 06:30:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational:
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2024-02-12 08:00:00
# severity level of the issue => [down|disrupted|notice]
severity: notice
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Storage


# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** {{< track "2022-08-24 11:35:00" >}} - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution.

section: issue
---

**Solved** {{< track "2024-02-12 08:00:00" >}} - The initialization of the
underlying RAID volume has been completed.

**Update** {{< track "2024-01-27 19:00:00" >}} - Ordering for accounts of type
staff, guest, stud and ueb are re-enabled via the [self-service
portal](https://support.ee.ethz.ch).

**Update** {{< track "2024-01-26 14:30:00" >}} - While the system is accessible
again, until the RAID volume will be fully initialized there might be further
performance penalty. While using the service keep in mind the [terms of use for
the service](https://computing.ee.ethz.ch/Services/NetScratch).

**Update** {{< track "2024-01-26 05:40:00" >}} - The structure to access the
[D-ITET NetScratch](https://computing.ee.ethz.ch/Services/NetScratch) has been
created to allow access to users. Links in the
[itet-stor](https://computing.ee.ethz.ch/Workstations/FindYourData) are
available again.

**Update** {{< track "2024-01-25 13:35:00" >}} - System is almost back in an
usable state for the [D-ITET
NetScratch](https://computing.ee.ethz.ch/Services/NetScratch).  The underlying
RAID volume needs to be initialized which will take several days. To give this
initialization enough priority to avance we will only release access to the
NetScratch on 2024-01-26.

**Update** {{< track "2024-01-25 11:25:00" >}} - Volumesets of the RAID are
initializing, which is a slow progress. Folder in `data-scratch-02` are
created.

**Update** {{< track "2024-01-25 10:45:00" >}} - Configuration verified and
RAID volumes created. Filesystems are beeing recreated.

**Update** {{< track "2024-01-25 09:45:00" >}} - Defective RAID Controller has
been replaced. We are verifying the setup and will then proceed with recreating
the service.

**Update** {{< track "2024-01-24 14:45:00" >}} - Data on `data-scratch-01`
([D-ITET NetScratch](https://computing.ee.ethz.ch/Services/NetScratch)) and
`data-scratch-02` are not recoverable. The filesystems will be re-created after
hardware replacement.

**Update** {{< track "2024-01-24 13:15:00" >}} - Given the suspect of the
malfunctioning Areca RAID Controller it will be replaced. The replacement
should be available tomorrow, 2024-01-25. After the configuration the
filesystem will be rebootstraped and made available again to the users.

**Update** {{< track "2024-01-24 09:40:00" >}} - The HW Raid-Controller hickup
combined with the high I/O load from the compute clusters has corrupted the
filesystem journaling metadata. At this point in time we have to assume that no
data will be possible to be recovered and the system will be built up again
after further investigation.

**Problem** {{< track "2024-01-24 07:15:00" >}} - The NetScratch server
is taken offline due to a filesystem corruption on the scratch data. The
system is currently not available for further access. To date data lost
cannot be ruled out.
