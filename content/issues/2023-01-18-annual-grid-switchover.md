---
# description of the subject in one sentence
title: Annual grid switchover in ETF building
# date/time the issue raised => [YYYY-MM-DD HH:MM:SS]
date: 2023-01-18 05:30:00
# set informational to [false] for initial maintenance announcements, leave it blank for all others issues
informational: false
# Has the issue been resolved? => [true|false]
resolved: true
# date/time the issue was resolved => [YYYY-MM-DD HH:MM:SS]
resolvedWhen: 2023-01-18 05:35:00
# severity level of the issue => [down|disrupted|notice]
severity:
# affected service(s) defined in cstate (e.g. E-Mail)
affected:
 - Network
 - ISG Services


# section describing the issue in more detail (descending order by date/time)
# possible key words (quoted in double asterisks)
# Problem     -> what is the problem, (side) effects for users, counter measures
# Update      -> for any kind of update important for the end users, e.g. when the issue might be resolved
# Solved      -> the issue has been solved
# Info        -> for any kind of information, e.g. planned maintenance (typically in the future)
# Maintenance -> for a ongoing maintenance
# Completed   -> for a completed maintenance work
#
# each entry closes with a time tag {{< track "YYYY-MM-DD HH:MM:SS" >}}
#
# Example: **Problem** - The ISG mail server is not available. It is no possible to send or receive emails. We are working on a solution. {{< track "2022-08-24 11:35:00" >}}

section: issue
---

**Completed** {{< track "2023-01-18 05:35:00" >}} - Grid switchover has been
performed and power is back to normal in buildings.

**Info** {{< track "2022-12-12 08:25:00" >}} - On Wednesday, January 18, 2023,
the Facility Services Department will conduct the annual grid switchover in the
ETF building. The grid switching will happen at 05:30. There will be no power
for approximately 10 seconds in the entire ET area (ETF, ETL, ETZ, GLC).
